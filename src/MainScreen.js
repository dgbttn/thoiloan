/**
 * Created by dgbttn on 04-Dec-18.
 */

var MainScreen = cc.Layer.extend({

    ctor:function() {
        this._super();
        //this.demo();
        this.initBackground();
    },

    demo:function() {
        var demo = new cc.Sprite(g_res.demo_png);
        demo.attr({
            anchorX: 0,
            anchorY: 0,
            x: 0,
            y: 0,
            scale: MW.BG.CURRENT_SCALE
        });
        this.addChild(demo,0);
    },

    initBackground:function() {
        var back = new BackGround();
        back.attr({
            anchorX: 0,
            anchorY: 0,
            x: 0,
            y: 0,
            scale: MW.BG.CURRENT_SCALE
        })
        this.addChild(back,0);
    }
});