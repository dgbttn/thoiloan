/**
 * Created by dgbttn on 04-Dec-18.
 */

var MW = {
    BG : {
        BASE_HEIGHT: 0,
        CURRENT_SCALE : 0.4,
        ISOMETRIC_SCALE: 0.49,
        ISOMETRIC_X: 270,
        ISOMETRIC_Y: 250
    }
};

var g_res = {
    isometricMap : "res/Art/Map/42x42map.tmx",
    demo_png : "res/Art/GUIs/Main_Gui/Demo.png",
    boundary : [
        'res/Art/Map/1_0001_Layer-1.png',
        'res/Art/Map/1_0003_Layer-2.png',
        'res/Art/Map/1_0000_Layer-3.png',
        'res/Art/Map/1_0002_Layer-4.png'
    ]
};
