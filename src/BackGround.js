/**
 * Created by dgbttn on 04-Dec-18.
 */

var BackGround = cc.Node.extend({

    ctor:function() {
        this._super();
        this.initDefaultMap();
    },

    initDefaultMap: function () {
        this.addGrass();
        this.addBoundary();
    },

    addGrass: function() {
        var isometricMap = new cc.TMXTiledMap.create(g_res.isometricMap);
        isometricMap.attr({
            anchorX: 0,
            anchorY: 0,
            x: MW.BG.ISOMETRIC_X,
            y: MW.BG.ISOMETRIC_Y,
            scale: MW.BG.ISOMETRIC_SCALE
        });
        this.addChild(isometricMap,0);
    },

    addBoundary: function () {
        var boundary = [];
        for (var i = 0; i<g_res.boundary.length; i++)
            boundary[i] = new cc.Sprite(g_res.boundary[i]);

        boundary[0].attr({
            x: 0,
            y: 0,
            anchorX: 0,
            anchorY: 0,
            scale: 1
        });
        boundary[1].attr({
            anchorX: 0,
            anchorY: 0,
            x: boundary[0].width-1,
            y: 0,
            scale: 1
        });
        boundary[2].attr({
            anchorX: 0,
            anchorY: 0,
            x: 0,
            y: boundary[0].height-1,
            scale: 1
        });
        boundary[3].attr({
            anchorX: 0,
            anchorY: 0,
            x: boundary[0].width-1,
            y: boundary[1].height-1,
            scale: 1
        });

        this.addChild(boundary[0],1);
        this.addChild(boundary[1],1);
        this.addChild(boundary[2],1);
        this.addChild(boundary[3],1);
    }
});
